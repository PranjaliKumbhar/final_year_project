import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
from PIL import Image
import os
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPool2D, Dense, Flatten, Dropout


data = []
labels = []
# We have 36 Classes (A-Z = 26 classes & 0-9 = 10 classes)
classes = 36


#preprocessing of image
for i in range(classes):
    path = os.path.join('E:\\A_last sem(2021)\\pr\\PR_mini_project\\char_dataset',str(i))
    images = os.listdir(path)
    #print(i)
    for a in images:
        try:
            image = Image.open(path + '\\' + a)
            #converting image into fixed size i.e 30*30
            image = image.resize((30,30))
            #converting image into array
            image = np.array(image)
            data.append(image)
            labels.append(i)
        except Exception as e:
            print(e)
            
data = np.array(data)
labels = np.array(labels) 

print(data.shape, labels.shape)

print(data.ndim)

#expand the shape of array (36576,30,30) ---> (36576,30,30,1)
data = np.expand_dims(data, -1)
print(data.ndim)

print(data.shape, labels.shape)

#normalize the pixel values (all values ranging between 0&1)
data = data/255

X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=0)

print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)

#class vector ---> binary vector
y_train = to_categorical(y_train, 36)
y_test = to_categorical(y_test, 36)

model = Sequential()
model.add(Conv2D(filters=32, kernel_size=(5,5), activation='relu', input_shape=X_train.shape[1:]))
model.add(Conv2D(filters=32, kernel_size=(5,5), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(rate=0.25))
model.add(Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
model.add(Conv2D(filters=64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(rate=0.25))
model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(rate=0.5))
# We have 36 classes that's why we have defined 36 in the dense
model.add(Dense(36, activation='softmax'))
model.summary()


#Compilation of the model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

epochs = 15
history = model.fit(X_train, y_train, batch_size=1050, epochs=epochs, validation_data=(X_test, y_test))

#save model
model.save('E:\\A_last sem(2021)\\pr\\PR_mini_project\\char_reg.h5')

# accuracy graph
plt.figure(0)
plt.plot(history.history['accuracy'], label='training accuracy')
plt.plot(history.history['val_accuracy'], label='val accuracy')
plt.title('Accuracy')
plt.xlabel('epochs')
plt.ylabel('accuracy')
plt.legend()
plt.show()


# Loss graph
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.title('Model Loss')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.legend()
plt.show()

# Classes of chracters and digits
classes = { 0:'A',1:'B', 2:'C',3:'D',4:'E',5:'F',6:'G',7:'H',8:'I', 9:'J', 10:'K',11:'L',12:'M',  
            13:'N',14:'O',15:'P',16:'Q',17:'R',18:'S',19:'T',20:'U',21:'V',22:'W',23:'X',24:'Y',
            25:'Z',26:'0',27:'1',28:'2',29:'3',30:'4',31:'5',32:'6',33:'7',34:'8',35:'9'
          }


#Recognition of new image
from PIL import Image
from sklearn.metrics import accuracy_score
import numpy as np
import matplotlib.pyplot as plt
def test_on_img(img):
    data=[]
    image = Image.open(img)
    image = image.resize((30,30))
    gray = image.convert('L')
    data.append(np.array(gray))
    data = np.expand_dims(data, -1)
    X_test=np.array(data)
    Y_pred = model.predict_classes(X_test)
    return image,Y_pred


plot,prediction = test_on_img(r'E:\\A_last sem(2021)\\pr\\PR_mini_project\\crop\\2.png')
s = [str(i) for i in prediction] 
a = int("".join(s)) 
print("Recognized Output: ", classes[a])
plt.imshow(plot,cmap="Greys")
plt.show()



fulltext = []
j=1
for i in range(9):
  plt.subplot(5,5,j); j+=1
  plot,prediction = test_on_img(r'E:\\A_last sem(2021)\\pr\\PR_mini_project\\crop\\'+str(i)+'.png')
  s = [str(i) for i in prediction] 
  a = int("".join(s)) 
  
  print("Recognized output: ", classes[a])
  fulltext.append(classes[a])
  plt.imshow(plot,cmap="Greys")
  plt.show()

print("Recognized output:",fulltext)

