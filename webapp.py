import os 
from pathlib import Path
from flask import Flask,request,render_template,send_from_directory,url_for
import cv2
from PIL import Image
from sklearn.metrics import accuracy_score
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import tensorflow as tf
from PIL import Image
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPool2D, Dense, Flatten, Dropout
from lib.text_detection import TextDetection
from lib.utils import plt_show
from lib.config import Config
import base64
import io

# Match contours to license plate or character template
def find_contours(dimensions, img) :

    # Find all contours in the image
    cntrs, _ = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Retrieve potential dimensions
    lower_width = dimensions[0]
    upper_width = dimensions[1]
    lower_height = dimensions[2]
    upper_height = dimensions[3]
    
    # Check largest 5 or  15 contours for license plate or character respectively
    cntrs = sorted(cntrs, key=cv2.contourArea, reverse=True)[:15]
    
    #ii = cv2.imread('contour.jpg')
    ii = cv2.imread('original.jpg')
    x_cntr_list = []
    target_contours = []
    img_res = []
    inx = 0 
    for cntr in cntrs :
        #detects contour in binary image and returns the coordinates of rectangle enclosing it
        intX, intY, intWidth, intHeight = cv2.boundingRect(cntr)
        
        #checking the dimensions of the contour to filter out the characters by contour's size
        if intWidth > lower_width and intWidth < upper_width and intHeight > lower_height and intHeight < upper_height :
            x_cntr_list.append(intX) #stores the x coordinate of the character's contour, to used later for indexing the contours
            char_copy = np.zeros((44,24))
            #extracting each character using the enclosing rectangle's coordinates.
            char = img[intY:intY+intHeight, intX:intX+intWidth]
            #cv2.imwrite('/content/drive/MyDrive/Colab Notebooks/crop/' + str(inx)+ '.png',char)
            char = cv2.resize(char, (20, 40))
            
            cv2.rectangle(ii, (intX,intY), (intWidth+intX, intY+intHeight), (50,21,200), 2)
            #plt.imshow(ii, cmap='gray')

           

            # Resize the image to 24x44 with black border
            char_copy[2:42, 2:22] = char
            char_copy[0:2, :] = 0
            char_copy[:, 0:2] = 0
            char_copy[42:44, :] = 0
            char_copy[:, 22:24] = 0

            img_res.append(char_copy) #List that stores the character's binary image (unsorted)
            inx = inx +  1
    #Return characters on ascending order with respect to the x-coordinate (most-left character first)
            
    #plt.show()
    #arbitrary function that stores sorted list of character indeces
    indices = sorted(range(len(x_cntr_list)), key=lambda k: x_cntr_list[k])
    img_res_copy = []
    for idx in indices:
        img_res_copy.append(img_res[idx])# stores character images according to their index
    img_res = np.array(img_res_copy)
  

    return img_res



# Find characters in the resulting images
def segment_characters(image) :

    # Preprocess cropped license plate image
    img_lp = cv2.resize(image, (333, 75))
    img_gray_lp = cv2.cvtColor(img_lp, cv2.COLOR_BGR2GRAY)
    _, img_binary_lp = cv2.threshold(img_gray_lp, 200, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    img_binary_lp = cv2.erode(img_binary_lp, (3,3))
    img_binary_lp = cv2.dilate(img_binary_lp, (3,3))
    

    LP_WIDTH = img_binary_lp.shape[0]
    LP_HEIGHT = img_binary_lp.shape[1]

   
    # Estimations of character contours sizes of cropped license plates
    dimensions = [LP_WIDTH/6,
                       LP_WIDTH/2,
                       LP_HEIGHT/10,
                       2*LP_HEIGHT/3]
    #plt.imshow(img_binary_lp, cmap='gray')
    #plt.show()
    cv2.imwrite('contour.jpg',img_binary_lp)
    cv2.imwrite('original.jpg',img_lp)

    # Get contours within cropped license plate
    char_list = find_contours(dimensions, img_binary_lp)

    idx = 0
    for i in char_list:
      cv2.imwrite('E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\crop\\' + str(idx)+ '.png',i)
      idx = idx +  1

    return char_list

classes = { 0:'A',1:'B', 2:'C',3:'D',4:'E',5:'F',6:'G',7:'H',8:'I', 9:'J', 10:'K',11:'L',12:'M',  
            13:'N',14:'O',15:'P',16:'Q',17:'R',18:'S',19:'T',20:'U',21:'V',22:'W',23:'X',24:'Y',
            25:'Z',26:'0',27:'1',28:'2',29:'3',30:'4',31:'5',32:'6',33:'7',34:'8',35:'9'
          }


webapp = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


print(APP_ROOT)

@webapp.route('/')

def index():
    return render_template('index.html')


import os
@webapp.route('/upload',methods=['POST'])
def upload():
    
    target = os.path.join(APP_ROOT,'images\\')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)
    else:
        print('Could not create upload directory:{}'.format(target))
    
    print(request.files.getlist('file'))
    
    for upload in request.files.getlist('file'):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "\\".join([target,filename])
        print("Accept incoming file",filename)
        print("Save it to:",destination)
        upload.save(destination)
        
        img = cv2.imread(destination)
        char = segment_characters(img)
        print("Image Segmentation Done")
        
        new_model = load_model('E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\char_reg.h5')
        new_model.summary()
        
        #from keras.preprocessing import image
        #image = image.load_img('images\\'+filename)
        
        
        
        
        l = len(os.listdir('E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\crop'))
        fulltext = []
        j=1
        for i in range(l):
            data = []
            image = Image.open('E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\crop\\'+str(i)+'.png')
            image = image.resize((30,30))
            gray = image.convert('L')
            data.append(np.array(gray))
            data = np.expand_dims(data, -1)
            X_test=np.array(data)
            Y_pred = new_model.predict_classes(X_test)
            s = [str(i) for i in Y_pred] 
            a = int("".join(s)) 
            print("Recognized output: ", classes[a])
            fulltext.append(classes[a])
            no_plate = "".join(fulltext)
  
        print("Recognized output:",fulltext)
        print("Number plate:",no_plate)
  
    
    return render_template('template.html',image_name=filename,text=no_plate)



@webapp.route('/upload_det',methods=['POST','GET'])
def det_upload():
    target = os.path.join(APP_ROOT,'images_vehicle\\')
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)
    else:
        print('Could not create upload directory:{}'.format(target))
    
    print(request.files.getlist('file'))
    
    for upload in request.files.getlist('file'):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = upload.filename
        destination = "\\".join([target,filename])
        print("Accept incoming file",filename)
        print("Save it to:",destination)
        upload.save(destination)
    
        img = destination

        config = Config()
        td = TextDetection(img, config, direction='both+',details=True)
        res=td.detect()
        cv2.imwrite('E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\' + 'final_img' + '.jpg' ,td.final)
        im = Image.open("E:\\A_last sem(2021)\\major_project\\final_project_detection_recognition\\final_img.jpg")
        data = io.BytesIO()
        im.save(data,"jpeg")
        encoded_img_data = base64.b64encode(data.getvalue())
        
    return render_template('detection_res.html',image_name1=filename,img_data=encoded_img_data.decode('utf-8'))

     
@webapp.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory('images', filename)

@webapp.route('/upload_det/<filename>')
def send_image_det(filename):
    return send_from_directory('images_vehicle', filename)


@webapp.route('/templates')
def img_reg():
    return render_template('recognize.html')

@webapp.route('/detection')
def img_det():
    return render_template('detection.html')

if __name__ == "__main__":
    webapp.run(port=3000,debug=True)      
        