import cv2
import matplotlib.pyplot as plt
import numpy as np

# Match contours to license plate or character template
def find_contours(dimensions, img) :

    # To find all contours in the image
    cntrs, _ = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Retrieve dimensions
    lower_width = dimensions[0]
    upper_width = dimensions[1]
    lower_height = dimensions[2]
    upper_height = dimensions[3]
    
    # Check largest contours for number plate respectively
    cntrs = sorted(cntrs, key=cv2.contourArea, reverse=True)[:15]
    
    #ii = cv2.imread('contour.jpg')
    ii = cv2.imread('original.jpg')
    x_cntr_list = []
    target_contours = []
    img_res = []
    inx = 0 
    for cntr in cntrs :
        #detects contour in binary image and returns the coordinates of rectangle 
        intX, intY, intWidth, intHeight = cv2.boundingRect(cntr)
        
        #checking the dimensions of the contour to filter out the characters by contour's size
        if intWidth > lower_width and intWidth < upper_width and intHeight > lower_height and intHeight < upper_height :
            x_cntr_list.append(intX) #stores the x coordinate of the character's contour
            char_copy = np.zeros((44,24))
            #extracting each character using the enclosing rectangle's coordinates.
            char = img[intY:intY+intHeight, intX:intX+intWidth]
            #cv2.imwrite('/content/drive/MyDrive/Colab Notebooks/crop/' + str(inx)+ '.png',char)
            char = cv2.resize(char, (20, 40))
            
            cv2.rectangle(ii, (intX,intY), (intWidth+intX, intY+intHeight), (50,21,200), 2)
            plt.imshow(ii, cmap='gray')

           

            # Resize the image to 24x44 with black border
            char_copy[2:42, 2:22] = char
            char_copy[0:2, :] = 0
            char_copy[:, 0:2] = 0
            char_copy[42:44, :] = 0
            char_copy[:, 22:24] = 0

            img_res.append(char_copy) #List that stores the character's binary image (unsorted)
            inx = inx +  1
    #Return characters in ascending order with respect to the x-coordinate
            
    plt.show()
    
    indices = sorted(range(len(x_cntr_list)), key=lambda k: x_cntr_list[k])
    img_res_copy = []
    for idx in indices:
        img_res_copy.append(img_res[idx])# stores character images according to their index
    img_res = np.array(img_res_copy)
  

    return img_res



# characters segmentation
def segment_characters(image) :

    # Preprocess number plate image
    
    #resizing
    img_lp = cv2.resize(image, (333, 75))
    #converting into gray image
    img_gray_lp = cv2.cvtColor(img_lp, cv2.COLOR_BGR2GRAY)
    #converting into binary image
    _, img_binary_lp = cv2.threshold(img_gray_lp, 200, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    #erosion on binarized image
    img_binary_lp = cv2.erode(img_binary_lp, (3,3))
    #dilating
    img_binary_lp = cv2.dilate(img_binary_lp, (3,3))
    

    LP_WIDTH = img_binary_lp.shape[0]
    LP_HEIGHT = img_binary_lp.shape[1]

   
    # Estimations of character contours sizes of cropped license plates
    dimensions = [LP_WIDTH/6,
                       LP_WIDTH/2,
                       LP_HEIGHT/10,
                       2*LP_HEIGHT/3]
    plt.imshow(img_binary_lp, cmap='gray')
    plt.show()
    cv2.imwrite('contour.jpg',img_binary_lp)
    cv2.imwrite('original.jpg',img_lp)

    # Get contours within cropped license plate
    char_list = find_contours(dimensions, img_binary_lp)
    
    #To store character and digits in crop folder
    idx = 0
    for i in char_list:
      cv2.imwrite('E:\\A_last sem(2021)\\pr\\PR_mini_project\\crop\\' + str(idx)+ '.png',i)
      idx = idx +  1

    return char_list



#Input image 
img = cv2.imread('E:\\A_last sem(2021)\\pr\\PR_mini_project\\Indian_Plates_testing\\6.png')
print ("Original Image")
char = segment_characters(img)
